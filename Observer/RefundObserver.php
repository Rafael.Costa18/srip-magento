<?php

namespace Sqrip\CustomPayment\Observer;

class RefundObserver implements \Magento\Framework\Event\ObserverInterface
{
    private $client;

    protected $scopeConfig;

    protected $customerRepository;

    protected $addressRepository;

    protected $storeManager;

    protected $countryCode;

    protected $language;

    protected $logger;

    public $request;

    public function __construct(\GuzzleHttp\Client                                   $client,
                                \Magento\Framework\App\Config\ScopeConfigInterface   $scopeConfig,
                                \Magento\Customer\Api\CustomerRepositoryInterface    $customerRepository,
                                \Magento\Customer\Api\AddressRepositoryInterface     $addressRepository,
                                \Magento\Store\Model\StoreManagerInterface           $storeManager,
                                \Sqrip\CustomPayment\Model\Config\Source\CountryCode $countryCode,
                                \Sqrip\CustomPayment\Model\Config\Source\Language    $language,
                                \Psr\Log\LoggerInterface                             $logger,
                                \Magento\Framework\App\RequestInterface              $request)
    {
        $this->client = $client;
        $this->scopeConfig = $scopeConfig;
        $this->customerRepository = $customerRepository;
        $this->addressRepository = $addressRepository;
        $this->storeManager = $storeManager;
        $this->countryCode = $countryCode;
        $this->language = $language;
        $this->logger = $logger;
        $this->request = $request;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $sqripRefundRequested = $this->request->getParam('sqrip_refund');
        $sqripRefundIban = $this->request->getParam('sqrip_refund_iban');

        if ($sqripRefundRequested) {
            $creditMemo = $observer->getEvent()->getCreditmemo();
            $amount = $creditMemo->getGrandTotal();

            $customer = $this->customerRepository->getById($creditMemo->getCustomerId());

            $shippingAddress = $this->addressRepository->getById($customer->getDefaultShipping());
            $shippingName = $shippingAddress->getFirstname() . $shippingAddress->getLastname();
            $shippingStreet = $shippingAddress->getStreet();
            $shippingCity = $shippingAddress->getCity();
            $shippingPostcode = $shippingAddress->getPostcode();
            $shippingCountryId = $shippingAddress->getCountryId();
            $shippingCompanyName = $shippingAddress->getCompany();

            $currencyCode = $this->storeManager->getStore()->getCurrentCurrency()->getCode();

            $dueDate = strtotime(date('Y-m-d') . " + " . $this->getScopeConfigValue('maturity') . " days");

            $countryCode = $this->countryCode->toArray()[$this->getScopeConfigValue('third_address_country_code')];
            $language = $this->language->toArray()[$this->getScopeConfigValue('language')];

            $orderId = $creditMemo->getOrder()->getIncrementId();

            $initialDigits = $orderId;

            $headers = [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key')
            ];
            $body = [
                "iban" => [
                    "iban" => $sqripRefundIban
                ],
                "payable_by" => [
                    "name" => $this->getScopeConfigValue('third_address_name'),
                    "street" => $this->getScopeConfigValue('third_address_street'),
                    "postal_code" => $this->getScopeConfigValue('third_address_zipcode'),
                    "town" => $this->getScopeConfigValue('third_address_city'),
                    "country_code" => "$countryCode"
                ],
                "payable_to" => [
                    "name" => $shippingCompanyName ?? $shippingName,
                    "street" => $shippingStreet[0],
                    "postal_code" => $shippingPostcode,
                    "town" => $shippingCity,
                    "country_code" => $shippingCountryId
                ],
                "payment_information" => [
                    "currency_symbol" => "$currencyCode",
                    "amount" => $amount,
                    "message" => $this->buildAdditionalInformation($this->getScopeConfigValue('additional_information'),
                        $dueDate,
                        $orderId),
                    "due_date" => date('Y-m-d', $dueDate),
                    "reference_is_set" => true
                ],
                "due_date" => date('Y-m-d', $dueDate),
                "lang" => "$language",
                "file_type" => "png",
                "product" => "Credit",
                "source" => "magento"
            ];

            if ($this->getScopeConfigValue('qr_code_basis') == 1) {
                $body['payment_information']['qr_reference'] = $initialDigits;
            }
            if ($this->getScopeConfigValue('initiate_qr_ref')) {
                $body['payment_information']['initial_digits'] = $this->getScopeConfigValue('initiate_qr_ref');
            }
            if ($this->getScopeConfigValue('payer') == 1) {
                $body['payable_by']['name'] = "$shippingName\n$shippingCompanyName";
            }
            if (strpos(strtolower($this->getScopeConfigValue('additional_information')), 'due_date format') !== false) {
                unset($body['payment_information']['due_date']);
            }

            $this->logger->debug('body', $body);

            $request = new \GuzzleHttp\Psr7\Request('POST', 'https://beta.sqrip.ch/api/code', $headers, json_encode($body));
            $response = $this->client->send($request);

            $response = json_decode((string)$response->getBody(), true);

            $fileUrl = $this->storeFile($response['png_file'], $creditMemo->getOrder()->getIncrementId());

            $this->logger->debug('$fileUrl');
            $this->logger->debug($fileUrl);
        }
    }

    private function buildAdditionalInformation($additionalInformation, $dueDate, $orderNumber)
    {
        $this->logger->debug('SqripApi->buildAdditionalInformation');

        $dateShortcodes = [];

        preg_match_all('/\[due_date format="(.*)"\]/', $additionalInformation, $dateShortcodes);
        foreach ($dateShortcodes[0] as $index => $dateShortcode) {
            $format = $dateShortcodes[1][$index];

            $dueDateFormat = ucwords(\IntlDateFormatter::formatObject(\DateTime::createFromFormat('U', $dueDate), $format));
            if (!$dueDateFormat) {
                continue;
            }

            $additionalInformation = str_replace($dateShortcode, $dueDateFormat, $additionalInformation);
        }

        return str_replace("[order_number]", $orderNumber, $additionalInformation);
    }

    private function storeFile($fileUrl, $orderId)
    {
        $this->logger->debug('SqripApi->storeFile');

        $response = $this->client->get(
            $fileUrl,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getScopeConfigValue('api_key'),
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]
        );

        $fileName = "refund-$orderId.png";

        $filesystem = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\Filesystem');
        $file = $filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)->openFile($fileName, 'w+');
        $file->lock();
        $file->write($response->getBody());
        $file->unlock();
        $file->close();

        $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');

        return $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $fileName;
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
