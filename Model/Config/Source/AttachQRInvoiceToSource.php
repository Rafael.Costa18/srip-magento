<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class AttachQRInvoiceToSource implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('New order')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => __('New order')];
    }
}
