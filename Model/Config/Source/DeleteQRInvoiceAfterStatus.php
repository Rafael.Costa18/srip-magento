<?php

namespace Sqrip\CustomPayment\Model\Config\Source;

/**
 * @api
 * @since 100.0.2
 */
class DeleteQRInvoiceAfterStatus implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 2, 'label' => __('Processing')], ['value' => 1, 'label' => __('Payment Review')], ['value' => 0, 'label' => __('Complete')]];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [0 => \Magento\Sales\Model\Order::STATE_COMPLETE, 1 => \Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW, 2 => \Magento\Sales\Model\Order::STATE_PROCESSING];
    }
}
