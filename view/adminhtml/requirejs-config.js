var config = {
    paths: {
        'select2': 'Sqrip_CustomPayment/js/select2.full.min',
    },
    shim: {
        'select2': {
            deps: ['jquery']
        }
    }
};
