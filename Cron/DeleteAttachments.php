<?php

namespace Sqrip\CustomPayment\Cron;

class DeleteAttachments
{
    private $logger;

    protected $filesystem;

    protected $orderRepository;

    protected $orderCollectionFactory;

    protected $deleteQRInvoiceAfterStatus;

    protected $scopeConfig;

    public function __construct(\Psr\Log\LoggerInterface                                            $logger,
                                \Magento\Framework\Filesystem                                       $filesystem,
                                \Magento\Sales\Api\OrderRepositoryInterface                         $orderRepository,
                                \Magento\Sales\Model\ResourceModel\Order\CollectionFactory          $orderCollectionFactory,
                                \Sqrip\CustomPayment\Model\Config\Source\DeleteQRInvoiceAfterStatus $deleteQRInvoiceAfterStatus,
                                \Magento\Framework\App\Config\ScopeConfigInterface                  $scopeConfig)
    {
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->orderRepository = $orderRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->deleteQRInvoiceAfterStatus = $deleteQRInvoiceAfterStatus;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute()
    {
        $this->logger->debug('cron is working...');

        $daysAgo = $this->getScopeConfigValue('delete_qr_invoice_after');
        $deleteAfterStatuses = $this->getScopeConfigValue('delete_qr_invoice_after_status');

        if (!empty($deleteAfterStatuses) && !empty($daysAgo) && $daysAgo != 0) {
            $orderStatuses = array_intersect_key($this->deleteQRInvoiceAfterStatus->toArray(), array_fill_keys($deleteAfterStatuses, true));

            $now = new \DateTime();
            $interval = new \DateInterval("P{$daysAgo}D");
            $interval->invert = 1;
            $targetDate = $now->add($interval)->format('Y-m-d H:i:s');

            $this->logger->debug('$targetDate');
            $this->logger->debug($targetDate);
            $this->logger->debug('$orderStatuses', $orderStatuses);

            $orderCollection = $this->orderCollectionFactory->create();
            $orderCollection->addFieldToFilter('status', ['in' => $orderStatuses])
                ->addFieldToFilter('created_at', ['lt' => $targetDate]);

            $directory = $this->filesystem->getDirectoryWrite(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
            foreach ($orderCollection as $order) {
                $orderId = $order->getIncrementId();

                $this->logger->debug('$orderId');
                $this->logger->debug($orderId);

                $this->logger->debug("deleting file-$orderId.pdf");

                $shopName = $this->scopeConfig->getValue(
                    'general/store_information/name',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $orderDate = date('Ymd');

                $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
                $fileName = str_replace("[order_number]", $orderId, $fileName);
                $fileName = str_replace("[order_date]", $orderDate, $fileName);
                $fileName = str_replace("[shop_name]", $shopName, $fileName);
                $fileName = str_replace(" ", "-", $fileName);

                $directory->delete($fileName);
            }
        }
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
