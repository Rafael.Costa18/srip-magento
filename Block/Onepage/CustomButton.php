<?php

namespace Sqrip\CustomPayment\Block\Onepage;

class CustomButton extends \Magento\Checkout\Block\Onepage\Success
{
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\View\Element\Template\Context   $context,
        \Magento\Sales\Model\Order\Config                  $orderConfig,
        \Magento\Framework\App\Http\Context                $httpContext,
        \Magento\Checkout\Model\Session                    $checkoutSession,
        array                                              $data = []
    )
    {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        $this->scopeConfig = $scopeConfig;
    }

    public function getCustomURL()
    {
        $orderId = $this->getOrderId();
        $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');

        $shopName = $this->scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $orderDate = date('Ymd');

        $fileName = $this->getScopeConfigValue('file_name') . '.pdf';
        $fileName = str_replace("[order_number]", $orderId, $fileName);
        $fileName = str_replace("[order_date]", $orderDate, $fileName);
        $fileName = str_replace("[shop_name]", $shopName, $fileName);
        $fileName = str_replace(" ", "-", $fileName);

        return $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . $fileName;
    }

    public function getOrderId()
    {
        return $this->_checkoutSession->getLastRealOrderId();
    }

    public function getOfferQRDownload()
    {
        return $this->getScopeConfigValue('offer_qr_download');
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
