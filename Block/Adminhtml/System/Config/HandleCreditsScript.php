<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class HandleCreditsScript extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <script>
        require(["jquery"], function($){
            jQuery("[id*=handle_credits_script]").hide();

            const testEmail = jQuery("#test-email");

            jQuery.ajax({
                method : "GET",
                url : "https://beta.sqrip.ch/api/details",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer ' . $this->getScopeConfigValue('api_key') . '"
                },
                showLoader: true,
                success: function(response) {
                    if(response) {
                        if(!response.free_credits && response.credits_left){
                            testEmail.after("<span>' . __('A paid-for credit will be charged') . '</span>");
                        }
                        if(!response.free_credits && !response.credits_left){
                            testEmail.after("<span>' . __('You have no credits left') . '</span>");
                            testEmail.attr("disabled", true);
                        }
                    }
                }
            });
        });
        </script>
        ';
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
