<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class HandleAddressScript extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <script>
        require(["jquery"], function($){
            jQuery("[id*=handle_address_script]").hide();

            const customPaymentAddressSelect = jQuery("select[id*=address_select]");
            const thirdAddress = jQuery("[id*=third_address_]");

            handleThirdAddress(customPaymentAddressSelect.val());

            customPaymentAddressSelect.on("change", function() {
                handleThirdAddress(jQuery(this).val());
            });

            function handleThirdAddress(selectedValue){
                if(selectedValue !== "2"){
                    thirdAddress.prop("readonly", true).attr("disabled", true);
                }
                else{
                    thirdAddress.prop("readonly", false).attr("disabled", false);
                }
            }
        });
        </script>
        ';
    }
}
