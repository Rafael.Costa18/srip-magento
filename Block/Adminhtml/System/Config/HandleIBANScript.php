<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class HandleIBANScript extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <script>
        require(["jquery"], function($){
            jQuery("[id*=handle_iban_script]").hide();

            jQuery("input[id*=address_qr_iban]").on("input", function() {
                const inputVal = jQuery(this).val();

                const tempInputVal = inputVal.replace(/ /g, "");

                const inputValFormatted =
                    tempInputVal.slice(0, 4) + " " +
                    tempInputVal.slice(4, 8) + " " +
                    tempInputVal.slice(8, 12) + " " +
                    tempInputVal.slice(12, 16) + " " +
                    tempInputVal.slice(16, 20) + " " +
                    tempInputVal.slice(20, 24) + " " +
                    tempInputVal.slice(24, 25);

                jQuery(this).val(inputValFormatted.trim());
            });
        });
        </script>
        ';
    }
}
