<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class TestIBANButton extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        parent::__construct($context);
        $this->scopeConfig = $scopeConfig;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        return '
        <button id="iban-test" type="button">' . __('Check') . '
        </button>
        <div class="sqrip-iban-notice mt-10 updated">
            <p>' . __('Validated') . '</p>
        </div>
        <div class="sqrip-iban-notice-description mt-10">
            <p>' . __('This is a QR IBAN. The customer can make payments only by specifying a QR reference (number). You can uniquely assign the deposit to a customer / order. This enables automatic matching of payments received with orders. Want to automate this step? Contact us <a href="mailto:info@sqrip.ch">info@sqrip.ch</a>.') . '</p>
        </div>
        <div class="sqrip-normal-iban-notice-description mt-10">
            <p>' . __('This is a normal IBAN. The customer can make deposits without noting the reference number (RF...). Therefore, automatic matching with orders is not guaranteed throughout. Manual processing may be necessary. A QR-IBAN is required for automatic matching. This is available for the same bank account. Information about this is available from your bank.') . '</p>
        </div>
        <div class="sqrip-iban-error-notice mt-10 updated">
            <p>' . __('Incorrect') . '</p>
        </div>
        <div class="sqrip-iban-error-notice-description mt-10">
            <p>' . __('The (QR-)IBAN of your account to which the transfer should be made is ERROR.') . '</p>
        </div>
        <script>
        require(["jquery"], function($){
            const apiKey = jQuery("textarea[id*=api_key]");
            const ibanKey = jQuery("input[id*=address_qr_iban]");
            const sqripNotice = jQuery("div.sqrip-iban-notice");
            const sqripErrorNotice = jQuery("div.sqrip-iban-error-notice");
            const sqripNoticeDescription = jQuery("div.sqrip-iban-notice-description");
            const sqripNormalNoticeDescription = jQuery("div.sqrip-normal-iban-notice-description");
            const sqripErrorNoticeDescription = jQuery("div.sqrip-iban-error-notice-description");
            const labelInitiateQRRef = jQuery("label[for*=initiate_qr_ref]");
            const inputInitiateQRRef = jQuery("input[id*=initiate_qr_ref]");

            checkIBAN(false);

            ibanKey.on("input", function(e){
                sqripNotice.hide();
                sqripNoticeDescription.hide();
                sqripNormalNoticeDescription.hide();
                sqripErrorNotice.hide();
                sqripErrorNoticeDescription.hide();
            });

            jQuery("#iban-test").on("click", function(e){
                checkIBAN(true);
            });

            function checkIBAN(showStatus){
                const apiKeyValue = apiKey.val();
                const ibanKeyValue = ibanKey.val();

                jQuery.ajax({
                method : "POST",
                url : "https://beta.sqrip.ch/api/validate-iban",
                headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + apiKeyValue
                },
                data: JSON.stringify({
                    "iban": {
                                "iban": "" + ibanKeyValue,
                                "iban_type": "simple"
                            }
                }),
                showLoader: true,
                beforeSend: function(xhr){
                },
                success: function(response) {
                    if(showStatus){
                        sqripNotice.show();
                        sqripErrorNotice.hide();
                        sqripErrorNoticeDescription.hide();
                        if(response.message === "Valid simple IBAN"){
                            sqripNormalNoticeDescription.show();
                        }
                        else{
                            sqripNoticeDescription.show();
                        }
                    }

                    if(response.message === "Valid simple IBAN"){
                        labelInitiateQRRef.text("Use these 6 characters (numbers or letters) as characters 5-10 in CR-references");
                        inputInitiateQRRef.next("span").remove();
                        inputInitiateQRRef.after("<span>This is an example of a CR reference with this option enabled, with X-es representing the 6 characters: RF18 XXXX XX12 1231 1231 4703 4</span>");
                        inputInitiateQRRef.addClass("simple-iban");
                        inputInitiateQRRef.removeClass("qr-iban");
                    }
                    else{
                        labelInitiateQRRef.text("' . __('Initiate QR-Ref# with these 6 digits') . '");
                        inputInitiateQRRef.next("span").remove();
                        inputInitiateQRRef.addClass("qr-iban");
                        inputInitiateQRRef.removeClass("simple-iban");
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ){
                    if(errorThrown){
                        if(showStatus){
                            sqripNotice.hide();
                            sqripNoticeDescription.hide();
                            sqripNormalNoticeDescription.hide();
                            sqripErrorNotice.show();
                            sqripErrorNoticeDescription.show();
                        }
                    }
                }
                });
            }
        });
        </script>
        ';
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
