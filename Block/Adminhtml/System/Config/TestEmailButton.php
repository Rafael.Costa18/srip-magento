<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\System\Config;

class TestEmailButton extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $scopeConfig;

    protected $objectManager;

    public function __construct(
        \Magento\Framework\ObjectManagerInterface          $objectManager,
        \Magento\Backend\Block\Template\Context            $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context);

        $this->scopeConfig = $scopeConfig;
        $this->objectManager = $objectManager;
    }

    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $url = $this->getUrl('custompayment/system_config/sendtestemail');
        $formKey = $this->objectManager->get(\Magento\Framework\Data\Form\FormKey::class)->getFormKey();

        return '
        <button id="test-email" type="button">' . __('Send Test Email') . '
        </button>
        <script>
        require(["jquery", "Magento_Ui/js/modal/alert"], function($, modalAlert){
            jQuery("#test-email").on("click", function(e){
                const sendTestEmailTo = jQuery("input[id*=send_test_email_to]");

                jQuery.ajax({
                    method : "GET",
                    url : "' . $url . '",
                    data: {
                        form_key: "' . $formKey . '",
                        return_session_messages_only: 1,
                        customer_email: sendTestEmailTo.val()
                    },
                    dataType: "json",
                    showLoader: true,
                    complete: function(response) {
                        modalAlert({
                            title: "' . __('Email Test') . '",
                            content: "' . __('Test email was sent <a href=\'https://url\' target=\'_blank\'>Click here</a> to view the invoice.') . '".replace("https://url", response.responseJSON.message)
                        });
                    }
                });
            });
        });
        </script>
        ';
    }

    private function getScopeConfigValue($key)
    {
        return $this->scopeConfig->getValue(
            "sqripqrinvoice/general/$key",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
