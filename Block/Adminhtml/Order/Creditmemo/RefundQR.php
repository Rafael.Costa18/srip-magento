<?php

namespace Sqrip\CustomPayment\Block\Adminhtml\Order\Creditmemo;

class RefundQR extends \Magento\Backend\Block\Template
{
    protected $_creditmemo;
    protected $_registry;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry             $registry,
        array                                   $data = []
    )
    {
        $this->_registry = $registry;

        parent::__construct($context, $data);
    }

    public function getQR()
    {
        $orderId = $this->getCreditMemo()->getOrder()->getIncrementId();

        $filesystem = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\Filesystem');
        $mediaDirectory = $filesystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
        $fileExists = $mediaDirectory->isExist("refund-$orderId.png");

        if ($fileExists) {
            $urlBuilder = \Magento\Framework\App\ObjectManager::getInstance()->get('\Magento\Framework\UrlInterface');
            $url = $urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA]) . "refund-$orderId.png";

            return '<img src="' . $url . '">';
        }

        return '';
    }

    public function getCreditMemo()
    {
        if (!$this->_creditmemo) {
            $this->_creditmemo = $this->_registry->registry('current_creditmemo');
        }

        return $this->_creditmemo;
    }
}
